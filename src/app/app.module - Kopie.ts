import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { URLSearchParams } from "@angular/http"

import { AppComponent } from './app.component';
import { FrontendComponent } from './frontend/frontend.component';

@NgModule({
  declarations: [
    AppComponent,
    FrontendComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Http,
    Response,
    Headers,
    RequestOptions
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
