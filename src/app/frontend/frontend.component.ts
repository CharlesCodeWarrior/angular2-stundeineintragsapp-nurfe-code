import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { URLSearchParams } from "@angular/http"
import { Router }			from '@angular/router';
import {dbService} from '../db-service/db-service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
})
export class FrontendComponent{
    showError: boolean;
    private user: string;
    private password: string;
  ;


    constructor(
        private router: Router,
        private dbService :dbService 
        ) {       
            

    }

    /** Login-Daten von FrontEnd zum backend senden , dort login anstossen Antwort auswerten 
     * login-daten in header rein, an slim/php senden
     * */
    login() {       
        //if login route to itemsList/stundeneinträge, else no route, error 
        if (this.dbService.login(this.user, this.password).subscribe(res => {console.log("res:"+res)})) {

            //#debug , #aendern!
            //this.router.navigate(['Stundeneintrags-Liste']);
            this.showError = false;
        } else {
            this.showError = true;
        }
        
    }


}
