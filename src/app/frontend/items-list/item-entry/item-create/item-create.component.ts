/*Idee/Funktionsweise: Alle Variablen binden mit [(ngModel)], um sie zu lesen/schreiben und in .ts datei zu übernehmen   
dort vars erstellen. 
eine createMethode nach Vorbild Login Methode
setze Http Request ab (werte stehen in: stufe0= header, stufe1= json array. php antwortet dann success true oder false im header/body) */

import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { URLSearchParams } from "@angular/http";
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  //styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent{

  user: string; 
  date: string;
  zuschlag: number;
  kdnr: string;
  auftrag: string;
  text: string;
  bill: string;
  showJSON: boolean;
  private bodyString: string;
  private data: any;
  private API_project_url: string; 
  private API_backend_url: string; 
  ;

  constructor(private http: Http) {       
      //this.API_project_url = 'http://localhost/my-code/Angular2/Zeiterfassung_alpha';     
      //this.API_project_url = 'http://localhost/my-code/Angular2/Zeiterfassung_stable';           
      this.API_project_url = 'http://localhost/my-code/Angular2/Zeiterfassung_alpha';
      this.API_backend_url = '/src/app/backend';
        
  }

        /** Prinzip der Methode: Login-Daten von FrontEnd zum backend senden , dort login anstossen Antwort auswerten 
        * login-daten in header rein, an slim/php senden
        @param form das form Objekt
        * */
        itemCreate(form:any) { 
        
        let headers = new Headers();
	    headers.append('user', this.user);
        //app-date da "date" nicht erlaubt ist/unsafe im header zb. in Chrome
        headers.append('app-date', this.date);
        console.log(form.value.dauer_select); //#debug
        let dauer = form.value.dauer_select;
        headers.append('duration', dauer);
        let zuschlagString =""+this.zuschlag;
        headers.append('zuschlag', zuschlagString);
        headers.append('kdnr', this.kdnr);
        headers.append('auftrag', this.auftrag);
        headers.append('text', this.text);
        //analog zu date
        headers.append('app-bill', this.bill);

        //string Verkettung für Gesamt-URL 
		let urlItemCreate = this.API_project_url +  this.API_backend_url + '/itemcreate.php/itemcreate';
		console.log("url: " + urlItemCreate);
        return this.http.get(urlItemCreate, { headers })
        .subscribe(res => {         /* Antwort auswerten */ /* '/itemcreate.php/itemcreate'; wegen der app-like-struktur von slim framework https://www.slimframework.com/docs/tutorial/first-app.html */
                this.data = res.text();
                this.showJSON = true;
                console.log("data: "+this.data);
                let headers = new Headers();
				headers = res.headers;
				console.log(" res.headers:"+headers); //debug
				let token = headers.get("token");
				console.log(" res.headers.token: "+token); //debug
            }, error => {
                console.log(error);
            });
    }

}
