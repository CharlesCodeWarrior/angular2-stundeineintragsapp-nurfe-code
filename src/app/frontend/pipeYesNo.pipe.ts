import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'pipeYesNo'})
export class pipeYesNo implements PipeTransform {
    transform(number:number) : any {
        let ergString='';
        if (number==0) {
            ergString='Ja';
        } else if (number ==1) {
            ergString='Nein';
        } else {
            ergString='ungueltig';
        }
        return ergString;
    }
}