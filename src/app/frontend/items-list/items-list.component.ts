import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { URLSearchParams } from "@angular/http";
import {pipeYesNo} from '../pipeYesNo.pipe';
import {AppComponent} from '../../app.component';
import { Observable } from 'rxjs/Observable';
import {dbService} from '../../db-service/db-service';
import { Injectable, EventEmitter } from "@angular/core";


@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  //styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit{
  showJSON: boolean;
  entryList: any;
  LoggedIn: boolean;
  observeable: Observable<any>;
  //für später evtl. wenn Daten sich im Betrieb ändern
  entryListChanged = new EventEmitter<[any]>();
  //API_backend_url = 'http://localhost/my-code/php/Zeiterfassung_Backend/';


    constructor(private http:Http, private dbService :dbService ) { 
      this.entryListChanged = new EventEmitter<[any]>();
    }


 ngOnInit() {
   this.LoggedIn = false;
   let obs = this.dbService.isLoggedIn();
   obs.subscribe(response => this.LoggedIn = response);
   console.log('LoggedIn: '+this.LoggedIn);
   this.getItemListFromDB();
   //will maybe be used later for data changes
   /* this.entryListChanged.subscribe(
        (entrLst: any) => {
          this.entryList = entrLst;
          console.log('entrLst aus ngInit: '+entrLst);
        });*/

 }

  getItemListFromDB(){
        this.showJSON = true;
            //this.observeable = this.http.get(this.API_backend_url+'login.php/itemsList');
            this.observeable = this.dbService.getEntryList();
            this.observeable.subscribe(res => { 
              this.entryList = res.json(); 
              if (this.entryList != null && this.entryList != undefined) {
                console.log('this.entryList: '+this.entryList);
                this.LoggedIn = true;
              }

            });
            
  }

  
  test() {
    console.log('test: '+JSON.stringify(this.entryList));
  }
}
