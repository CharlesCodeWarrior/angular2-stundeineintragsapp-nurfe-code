import { Injectable, EventEmitter } from '@angular/core';
import { Component, OnInit, OnChanges,SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { URLSearchParams } from "@angular/http"
//import {confConst} from './conf';
//import conf = require('./conf');
import 'rxjs/add/operator/map';


@Injectable()

@Component({
    selector: 'debug-db-service-Component',
    templateUrl: './debug-db-service.html',
})

    export class dbService implements OnInit, OnChanges{
    public token: string;
    private API_project_url; 
    private API_backend_url;
    //private apiBE_url = confConst.API_project_url+confConst.API_backend_url;
    private apiBE_url = "";
    user: string;
    showJSON: boolean;
    private password: string;
    private password2: string;
    private bodyString: string;
    private data: any;
    private data2: any;
    entryList: any;
    observable : Observable<Response>;
    entryListChanged = new EventEmitter<any>();


  ngOnInit () {
    console.log(`onInit: `);
    console.log(`user: `+this.user+' password: '+this.password+' token: '+this.token);
  }  

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
        let chng = changes[propName];
        let cur  = JSON.stringify(chng.currentValue);
        let prev = JSON.stringify(chng.previousValue);
        console.log(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
    }
    console.log(`onChange: `);
    console.log(`user: `+this.user+' password: '+this.password+' token: '+this.token);
  }



  constructor(private http:Http) { 
    // set token if saved in local storage
		var currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.token = currentUser && currentUser.token;
        this.API_project_url = 'http://localhost/my-code/Angular2/Zeiterfassung_alpha';
        this.API_backend_url = '/src/app/backend';
        this.apiBE_url = this.API_project_url+this.API_backend_url;       
  }


  /** Login-Daten von FrontEnd zum backend senden , dort login anstossen Antwort auswerten 
     * login-daten in header rein, an slim/php senden
     * */
    
    login(username, password) : Observable<boolean> {        
    let headers = new Headers();
		headers.append('user', username);
		headers.append('password', password);
    let loggedIn = false;
    let token = null;         
        //string Verkettung für Gesamt-URL 
        //get a Observable response, map response to a function
        //which extracts data , checks and returns true/false for the Observable boolean
        return this.http.get(this.apiBE_url + '/login.php/login', { headers })
            .map(res => {          //Antwort auswerten login.php/login wegen der app-like-struktur von slim framework, siehe: https://www.slimframework.com/docs/tutorial/first-app.html 
                
            this.data2 = res.text();
            console.log("responseText: "+this.data2);            
                if (res.ok) {
                  console.log("asssh"+res.ok);
                  console.log("res.json:"+ JSON.stringify(res.json() ) );
                  console.log("token:"+ res.json() );
                  let jsonObject : JSON;
                  jsonObject = res.json();
                console.log("res.json2Test"+ JSON.stringify(res.json()[0].token));
                    /*JSON is in Short an Array of Objects. res.json:[object Object]
                    res.json[{token:value, user:value}]
                    means an array with an object of type Object, this Object then can be 
                    identified /read by obj notation obj.attribute*/
                  token = res.json()[0].token;                
                  
                  	if (token) {
                        // set token property
                        this.token = token;
                        // store jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentUser', JSON.stringify({ token: token }));
                        loggedIn = true;
                         return true;
                    }
                     else {
                        // return false to indicate failed login
                        loggedIn = false;
                        return false;
                     }
                }                
            }, error => {
                console.log(error);
                loggedIn = false;
            });
         
    }  

   
    logout(): void {
		// clear token remove user from local storage to log user out
		this.token = null;
		localStorage.removeItem('currentUser');
	}


    /** save token locally when user logs in. check if loggedIn via token sending between Frontend and Backend. purpose eg: check user id and show his entrylist(Stundeneintragliste) and so on.
     */
    isLoggedIn(): Observable<boolean> {
		let headers = new Headers();
		headers.append('token', this.token);
        //ask Backend if user is logged via get request to backend via php method. the response is an Observable		
        /*#debug */ this.http.get(this.apiBE_url + '/login.php/isLoggedIn', { headers }).subscribe(responseObservable => { console.log("responseObservable: "+responseObservable)}); 
        /*map the responseObservable to a responseObservableBool,  the responseObservable is coming from an Observable from the get Response,mapping via a function which extracts token , extracts username and finally returns in the responseObservableBool true or false.  */
			return this.http.get(this.apiBE_url + '/login.php/isLoggedIn', { headers }).map((response: Response) => {
				let headers = new Headers();
				headers = response.headers;
				console.log(headers); //debug
				let token = headers.get("token");
				this.user = headers.get("username");
				console.log(token); //debug
                //if ok save locally
				if (token) {
					this.token = token;
					localStorage.setItem('currentUser', JSON.stringify({ token: token }));
					return true;
				} else {
					return false;
				}
			});
	}

    /** gets getUserName. avoid an undefined response by calling login() or isLoggedIn() before 
     * 
     */
    getUserName(): string {
        return this.user;
    }


    /** gets EntryList, holt Liste der Stundeneinträge,  these are fetched over a REST conform  http.get, backend
     * @returns Observable, where you can subscribe to, which then gets the data (einschreiben, Änderungen mitbekommen, daten rausziehen)         
     */
    getEntryList(): Observable<any> {
        /*//Kurzschreibweise wie diese hier wird hier in verständlicheren längeren Code aufgelöst.löst Zugriffsprobleme
             this.http.get(this.API_backend_url+'itemsListSlim.php')
              .subscribe(res => { this.entryList = res.json();
              console.log(this.entryList);}); //anymous functions     */  
            this.showJSON = true;
            //einen Observable also ein Beobachter holen
            console.log(this.apiBE_url+'/login.php/itemsList');
            this.observable = this.http.get(this.apiBE_url+'/login.php/itemsList');//      
            return this.observable; 
    } 


     /*CreateUserHttpPostUnsafe(){
        console.log("u:"+this.user+" p:"+this.password+"p2 "+this.password2);
        let body = new URLSearchParams();
        body.append('user', this.user);
        body.append('password', this.password);
        body.append('password2', this.password2);

        console.log(body);

        this.http
            .post('http://localhost/my-code/Angular2/Zeiterfassung/app/backend/slim/register.php?register=1', body)
            .subscribe(res => {
                this.data2 = res.json()
                    this.showJSON2 = true;
                    console.log(this.data2);
            }, error => {
                console.log(error.json());
            });

    }   */


}
