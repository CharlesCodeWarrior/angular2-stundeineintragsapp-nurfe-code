import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {FrontendComponent} from './frontend/frontend.component';
import {ItemCreateComponent} from './frontend/items-list/item-entry/item-create/item-create.component';
import {ItemsListComponent} from './frontend/items-list/items-list.component';

const appRoutes: Routes = [
    {
        path:'',
        component: FrontendComponent
    },
    {
        path: 'Stundeneintrag-erstellen',
        component: ItemCreateComponent
    },
    {
        path: 'Stundeneintrags-Liste',
        component: ItemsListComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);