/* If you export an object which contains all the constants, you could simply use es6 import the module without using require.
I also used Object.freeze to make the properties become true constants.   */

export const confConst = Object.freeze({
     //this.API_project_url = 'http://localhost/my-code/Angular2/Zeiterfassung_stable';
    //API_backend_url muss momentan in jeder betreffenden Klasse gesetzt werden. Suche in Dateien machen bitte
    //API_backend_url:please set in all classes, where it is used. Use search in files in your IDE
    API_project_url : 'http://localhost/my-code/Angular2/Zeiterfassung_alpha', 
    API_backend_url : '/src/app/backend'
 });