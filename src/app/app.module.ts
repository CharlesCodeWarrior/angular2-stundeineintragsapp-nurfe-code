import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppComponent }  from './app.component';
import { FrontendComponent }  from './frontend/frontend.component';
import { ItemsListComponent } from './frontend/items-list/items-list.component';
import { ItemEntryComponent } from './frontend/items-list/item-entry/item-entry.component';
import { ItemCreateComponent } from './frontend/items-list/item-entry/item-create/item-create.component';
import {pipeYesNo} from './frontend/pipeYesNo.pipe';
import {routing} from './app.routing';
import { dbService } from './db-service/db-service';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, routing  ],
  declarations: [ AppComponent, FrontendComponent, ItemsListComponent, ItemEntryComponent, ItemCreateComponent, pipeYesNo], 
  providers: [ dbService ],
  bootstrap:    [ AppComponent]
})
export class AppModule { }