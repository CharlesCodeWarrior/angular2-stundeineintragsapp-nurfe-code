import { Component } from '@angular/core';
import {FrontendComponent} from './frontend/frontend.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	/*Stundeneintrags App/App for Organazing Work Hours at Customers
	- FrontEnd Angular2
	- Middleware PHP-Slimframework, Takes request, evaluates it with Backend,
		sends Response to FrontEnd
	- Backend in php with Methods in PHP-Slimframework
	- Alpha Status, functional, 80 % Complete
	- GUI for creating entries needs a bit overhaul
		- datepicker (bootstrap-datepicker does not work in Angular2, tried alternatives 1/2
	*/
  title = 'Hey Charles :-) App works!';  
}
